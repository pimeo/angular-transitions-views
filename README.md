# angular-transitions-views

Gestion des animations entre deux vues

- Un service de transition par vue comprenant une transition d'entrée et une transition de sortie.
- Une réécriture de la directive ng-view pour la gestion des évènements.
- Une directive pour chaque template avec l'id de la vue en argument de directive.


## Version

version actuelle : 0.1 (30/07/2013)


