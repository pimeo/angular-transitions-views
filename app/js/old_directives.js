'use strict';

/* Directives */


angular.module('myApp.directives', [])
  
  .directive('appVersion', ['version', function(version) {
    return function(scope, elm, attrs) {
      elm.text(version);
    };
  }])

  .directive('appView', ['$route', '$rootScope', '$compile', function ($route, $rootScope, $compile) {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        console.log('appView');
        $rootScope.$on('$view.transitionIn', transitionIn);
        $rootScope.$on('$view.transitionOut', transitionOut);


        function transitionIn(evt, data){
          if( evt.defaultPrevented ) return;
          evt.preventDefault();
          
          console.log('transition in')
          var args = {onComplete: transitionInComplete, paused: true};
          var tm = new TimelineMax(args);

          switch(data.view){
            case 'home':
                tm.to($('#home'), 3, {opacity: 1});
              break;
            case 'article':
                tm.to($('#article'), 3, {opacity: 1});
              break;
          }

          tm.play();
        }

        function transitionInComplete(){
          $rootScope.$emit('$view.transitionInComplete');
        }

        function transitionOut(evt, data){
          console.log(evt);
          if( evt.defaultPrevented ) return;
          evt.preventDefault();



          console.log('transition out')
          var args = {onComplete: transitionOutComplete, paused: true};
          var tm = new TimelineMax(args);
          console.log('herere', data.view.substring(1));
          switch(data.view.substring(1)){
            case 'home':
                tm.to($('#home'), 3, {opacity: 0});
              break;
            case 'article':
                tm.to($('#article'), 3, {opacity: 0});
              break;
          }

          tm.play();
        }

        function transitionOutComplete(){
          console.log('transition out complete')
          $rootScope.$emit('$view.transitionOutComplete');
        }
      }
    };
  }]);
