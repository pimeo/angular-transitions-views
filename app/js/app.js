'use strict';


// Declare app level module which depends on filters, and services
angular.module('myApp', ['myApp.filters', 'pascalprecht.translate', 'myApp.services', 'myApp.directives', 'myApp.controllers', 'myApp.transitions'])
  .run(['$rootScope', function($rootScope){
  	// run instructions
  }])
  .config(['$routeProvider', '$translateProvider', function($routeProvider, $translateProvider) {
    $routeProvider.when('/home', {templateUrl: 'partials/home.html', controller: 'HomeCtrl'});
    $routeProvider.when('/article', {templateUrl: 'partials/article.html', controller: 'ArticleCtrl'});
    $routeProvider.otherwise({redirectTo: '/home'});

    $translateProvider.useStaticFilesLoader({
		prefix: 'i10n/',
		suffix: '.json'
	});

	$translateProvider.uses('fr_FR');

  console.log($translateProvider)
  }]);
