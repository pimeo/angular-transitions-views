'use strict';

angular.module('myApp.transitions', [])

.factory('homeTransition', function () {
  return {
    tm : '', 
    time: 0,
    args: {},

		// animation enter
		enter: function (){
			console.log('home enter transition')
			this.args = {};
			console.log(this.args);
			this.tm = new TimelineMax(this.args);
			this.tm
				.to($('#home'), 2, {opacity: 1}, 0)
				.to($('#title'), 2, {marginLeft: 0}, 1)
				.to($('#content'), 2, {marginLeft: 0}, 1.5);
			
		},

		// animation leave
		leave: function(done){
			this.args = {onComplete:done};
			this.tm = new TimelineMax(this.args);
			this.tm
				.to($('#home'), 2, {opacity: 0})
				.to($('#title'), 2, {marginLeft: -210}, 0.2)
				.to($('#content'), 2, {marginLeft: -210}, 0.3);
		}
  };
})
.factory('articleTransition', function () {
  return {
    tm: '', 
    time: 0,
    args: {},

		// animation enter
		enter: function (){
			console.log('article enter transition')
			this.args = {};
			this.tm = new TimelineMax(this.args);
			this.tm.to($('#article'), 2, {opacity: 1});
		},

		// animation leave
		leave: function(done){
			this.args = {onComplete:done};
			this.tm = new TimelineMax(this.args);
			this.tm.to($('#article'), 2, {opacity: 0}, 0);
		}
  };
});