'use strict';

/* Controllers */

angular.module('myApp.controllers', [])
  .controller('AppCtrl', ['$scope', '$translate', '$rootScope', '$location', function($scope, $translate, $rootScope, $location) {

    $rootScope.noTransitionOut = false;
    
    $scope.test = 'bonjout';

    $scope.changeLanguage = function(key){
      $rootScope.noTransitionOut = true;
      $translate.uses(key)
      $location.path('/home');
      console.log($location.path() );
    }

  }])
  .controller('HomeCtrl', [function() {

  }])
  .controller('ArticleCtrl', [function() {

  }]);



/*

  ------------------
  Events : 
  ------------------

  $scope :: $view.transitionOut
  $scope :: $view.transitionOutComplete

  $scope :: $view.transitionIn
  $scope :: $view.transitionInComplete

  $scope.pageDestroy
  $scope.pageReady

  $rootScope :: $viewContentLoaded


*/