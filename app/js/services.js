'use strict';

/* Services */


// Demonstrate how to register services
// In this case it is a simple value service.
angular.module('myApp.services', [])
  .value('version', '0.1')
  .service('transitionManager', ['$rootScope', function($rootScope){
  	return {
  		broadcast: function(event, args){
  			$rootScope.$broadcast(event, args);
  		}
  	}
  }]);
