'use strict';

/* Directives */


angular.module('myApp.directives', [])
  
  .directive('appVersion', ['version', function(version) {
    return function(scope, elm, attrs) {
      elm.text(version);
    };
  }])
   .directive('appPage', 
    ['$route', '$rootScope', 'homeTransition', 'articleTransition', '$timeout',
    function ($route, $rootScope, homeTransition, articleTransition, $timeout) {
      return {
        restrict: 'A',
        link: function (scope, element, attrs) {

          // Emitted every time the ngView content is reloaded.
          scope.$on('$viewContentLoaded', viewContentLoaded);
          scope.$on('$page.transitionIn', trIn);
          scope.$on('$page.transitionOut', transitionOut);

          function viewContentLoaded(){
            // displayContent
            transitionIn();
          }


          function trIn(){
             console.log('trIn', attrs.appPage)
          }

          function transitionOut(evt){
              if( evt.defaultPrevented ) return;
              evt.preventDefault();
              console.log('trOut', attrs.appPage);

              console.log('transition :: ', $rootScope.noTransitionOut);

              // no transition out
              if( $rootScope.noTransitionOut ){
                $rootScope.noTransitionOut = false;
                done();
              }else{
                switch( attrs.appPage ){
                  case 'home': 
                      homeTransition.leave(done);
                    break;
                  case 'article':
                      articleTransition.leave(done);
                    break;
                  default: console.log("view doesn't exist");
                }
              }
          }

          function done(){
            console.log('done!');
            $timeout(function(){
              scope.$emit('$page.transitionOutComplete');
            })
          }

          function transitionIn(){
            switch( attrs.appPage ){
              case 'home':
                  homeTransition.enter();
                break;
              case 'article':
                  articleTransition.enter();
                break;
              default: console.log("view doesn't exist");
            }
          }     
        }
      };
    }
  ])
  .directive('appView', 
    ['$http', '$templateCache', '$route', '$anchorScroll', '$compile', '$controller', '$rootScope', '$timeout',
    function($http,   $templateCache,   $route,   $anchorScroll,   $compile, $controller, $rootScope ,$timeout) {
      return {
        restrict: 'ECA',
        terminal: true,
        link: function(scope, element, attr) {
          var lastScope,
              onloadExp = attr.onload || '';

          scope.$on('$routeChangeSuccess', update);
          update();


          function destroyLastScope() {
            if (lastScope) {
              lastScope.$destroy();
              lastScope = null;
            }
          }

          function clearContent() {
            element.html('');
            destroyLastScope();
          }

          function update() {
            console.log('UPADTE')
            var locals = $route.current && $route.current.locals,
            template = locals && locals.$template;

            if (template) {

               if( lastScope ){
                lastScope.$broadcast('$page.transitionOut');

                lastScope.$on('$page.transitionOutComplete', function(evt){
                  if( evt.defaultPrevented ) return;
                  evt.preventDefault();

                  console.log('transition out here')

                  clearContent();

                  element.html(template);

                  var link = $compile(element.contents()),
                      current = $route.current,
                      controller;

                  lastScope = current.scope = scope.$new();
                  if (current.controller) {
                    locals.$scope = lastScope;
                    controller = $controller(current.controller, locals);
                    element.children().data('$ngControllerController', controller);
                  }

                  link(lastScope);
                  
                  lastScope.$emit('$viewContentLoaded');
                  //lastScope.$eval(onloadExp);
                  lastScope.$apply();
                });
               
              }
              else{
                clearContent();
                
                element.html(template);

                var link = $compile(element.contents()),
                    current = $route.current,
                    controller;

                lastScope = current.scope = scope.$new();
                if (current.controller) {
                  locals.$scope = lastScope;
                  controller = $controller(current.controller, locals);
                  element.children().data('$ngControllerController', controller);
                }

                link(lastScope);
                lastScope.$emit('$viewContentLoaded');
                lastScope.$eval(onloadExp);
              }
              
              // $anchorScroll might listen on event...
              //$anchorScroll();
            } else {
              clearContent();
            }
          }
        }
      };
    }
  ]);